/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.keaplogik.examples.model.animals.impl;

import com.keaplogik.examples.model.animals.Animal;

/**
 *
 * @author keaplogik
 */
public interface Mammel extends Animal {

    int getNumOfLegs();
}
